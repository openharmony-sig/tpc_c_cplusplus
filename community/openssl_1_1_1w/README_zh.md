# openssl三方库说明
## 功能简介
OpenSSL是一个强大的、商业级的、功能齐全的用于传输层安全（TLS）协议的开源工具包，以前称为安全套接字层（SSL）协议，应用程序可以使用这个包来进行安全通信，避免窃听，同时确认另一端连接者的身份。

## 三方库版本
- OpenSSL_1_1_1w

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
