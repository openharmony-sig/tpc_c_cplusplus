# fontconfig三方库说明
## 功能简介
Fontconfig是字体相关的计算机程序库，用于配置、定制全系统的字体，或将字体提供给应用程序使用。

## 三方库版本
- 2.14.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
