# tcl三方库说明
## 功能简介
tcl是一种解释语言，也是该语言的一种非常便携的解释器。

## 三方库版本
- 8.6.13

## 已适配功能
- 支持tcl解释器

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
