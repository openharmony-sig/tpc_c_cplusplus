# zstd 三方库说明
## 功能简介
zstd 是一种快速的无损压缩算法，是针对 zlib 级别的实时压缩方案，以及更好的压缩比。

## 三方库版本
- v1.5.6

## 已适配功能
- 完成了生成和解码 .zst 格式以及字典压缩、解压缩

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)