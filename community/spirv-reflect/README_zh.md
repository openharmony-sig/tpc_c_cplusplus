# spirv-reflect三方库说明
## 功能简介
SPIRV-Reflect是一个轻量级库，它为Vulkan应用程序中的spirv着色器字节码提供了C/c++反射API。
## 三方库版本：
- sdk-1.3.204.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)