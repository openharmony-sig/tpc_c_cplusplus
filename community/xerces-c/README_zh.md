# xerces-c三方库说明
## 功能简介
xerces是一个开放源代码的XML语法分析器，它提供了SAX和DOM API。

## 三方库版本
- v3.2.4

## 已适配功能
- 支持sax解析和dom解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
