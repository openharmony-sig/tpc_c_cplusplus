# sqliteodbc三方库说明
## 功能简介
sqliteodbc是基于SQLite数据库的ODBC驱动程序

## 三方库版本
- 0.9998

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
