# InferLLM三方库说明

## 功能简介
InferLLM 是一个简单高效的 LLM CPU 推理框架，可以实现在本地部署 LLM 中的量化模型

## 三方库版本
- 405d866e4c11b884a8072b4b30659c63555be41d

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

+ [应用hap包集成](docs/hap_integrate.md)
