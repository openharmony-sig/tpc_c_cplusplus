# reactphysics3d三方库说明
## 功能简介
reactphysics3d是一个开源的c++物理引擎库，可以用于3D模拟和游戏。
## 三方库版本：
- 0.9.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)