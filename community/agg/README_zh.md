# Anti-Grain Geometry 三方库说明

## 功能简介
Anti-Grain Geometry 是一个用C++编写的开源二维矢量图形库。

## 三方库版本
- 2.6

## 已适配功能
- 二维矢量图形处理。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](docs/hap_integrate.md)
