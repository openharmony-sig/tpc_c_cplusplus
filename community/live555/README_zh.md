# live555三方库说明

## 功能简介

互联网流媒体、无线和组播技术、服务和标准

## 三方库版本
- 2c92a57ca04b83b2038ab2ab701d05a54be06a85

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
