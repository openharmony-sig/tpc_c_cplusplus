# nanodbc 三方库说明
## 功能简介
nanodbc 是一个用于C++的轻量级、头文件驱动的ODBC库封装，它提供了一个简单而现代的接口来访问ODBC数据库。

## 三方库版本
- v2.13.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
