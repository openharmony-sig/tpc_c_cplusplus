# zopfli三方库说明
## 功能简介
Zopfli压缩算法是一个用C语言编写的压缩库

## 三方库版本
- 1.0.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
