# meshoptimizer三方库说明
## 功能简介
meshoptimizer是一个网格优化库，使网格更小，更快地渲染
## 三方库版本：
- 0.19
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)