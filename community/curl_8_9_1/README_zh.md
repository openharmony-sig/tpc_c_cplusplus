# curl_8_9_1三方库说明
## 功能简介
curl_8_9_1是一个C库用于网络请求。

## 三方库版本
- curl-8_9_1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
