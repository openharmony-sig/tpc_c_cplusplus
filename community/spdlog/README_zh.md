# spdlog 三方库说明
## 功能简介
spdlog是一个 非常快，头文件/编译，c++日志库

## 三方库版本
- v1.9.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
