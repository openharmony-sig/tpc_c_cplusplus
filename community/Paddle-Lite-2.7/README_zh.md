# Paddle-Lite 三方库说明
## 功能简介
paddlelite是深度神经网络学习框架。

## 三方库版本
- v2.7

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
