# unrar三方库说明
## 功能简介
unrar是一个解压rar文件的库。

## 三方库版本
- v1.0.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
