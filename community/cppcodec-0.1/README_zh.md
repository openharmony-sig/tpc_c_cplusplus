# cppcodec-0.1三方库说明
## 功能简介
cppcodec-0.1是仅标头的c++ 11库，用于编码/解码RFC 4648中指定的base64, base64url, base32, base32hex和hex。
## 三方库版本：
- v0.1
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
