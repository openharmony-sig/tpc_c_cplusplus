# lpg4z三方库说明
## 功能简介
Log4z是一个开源的c++轻量级跨平台日志库。
## 三方库版本：
- v3.4.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
