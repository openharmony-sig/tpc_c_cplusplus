# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Contributor: wangjialun<2271411@stu.neu.edu.cn>, zhangqian<2371418@stu.neu.edu.cn>, wangyihao<2942375747@qq.com>, wangying<wangying@swc.neu.edu.cn>
# Maintainer: wangyihao<2471389@stu.neu.edu.cn>, wangjialun<2271411@stu.neu.edu.cn>, zhangqian<2371418@stu.neu.edu.cn>, wangying<wangying@swc.neu.edu.cn>

pkgname=log4z-3.4.0
pkgver=v3.4.0
pkgrel=0
pkgdesc="Log4z is an open source C++ lightweight & cross platform log library."
url="https://github.com/zsummer/log4z/tree/v3.4.0"
archs=("armeabi-v7a" "arm64-v8a")
license=("MIT license")
depends=()
makedepends=()
source="https://github.com/zsummer/log4z/archive/refs/tags/v3.4.0.zip"

downloadpackage=true
autounpack=true
buildtools=cmake
patchflag=true

builddir=log4z-3.4.0
packagename=log4z-v3.4.0.zip

# 为编译设置环境，如设置环境变量，创建编译目录等
prepare() {
    if $patchflag
    then
        cd $builddir
        patch -p1 < `pwd`/../log4z-3.4.0_oh_pkg.patch
        # patch只需要打一次,关闭打patch
        patchflag=false
        cd $OLDPWD
    fi
    mkdir -p $builddir/$ARCH-build
}

build() {
    cd $builddir
    ${OHOS_SDK}/native/build-tools/cmake/bin/cmake "$@" -DCMAKE_C_FLAGS="-Wno-unused-command-line-argument" \
        -DCMAKE_CXX_FLAGS="-Wno-unused-command-line-argument" -B$ARCH-build -S./ > $buildlog 2>&1
    $MAKE VERBOSE=1 -C $ARCH-build >> $buildlog 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
}

package() {
    cd $builddir
    $MAKE -C $ARCH-build install >> $buildlog 2>&1
    cd $OLDPWD
}


check() {
    echo "The test must be on an OpenHarmony device!"
}


cleanbuild() {
    rm -rf ${PWD}/$builddir
}