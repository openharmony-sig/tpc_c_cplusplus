# bullet3三方库说明
## 功能简介
bullet3可以实现实时碰撞检测和多物理场仿真，适用于VR、游戏、视觉效果、机器人、机器学习等。
## 三方库版本：
- 3.22
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
