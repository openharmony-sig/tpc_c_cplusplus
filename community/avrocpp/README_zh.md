# avrocpp三方库说明
## 功能简介
avrocpp是指数据序列化的系统，有丰富的数据结构类型、快速可压缩的二进制数据形式。

## 三方库版本
- release-1.11.1

## 已适配功能
- 提供数据的序列化框架

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)