# ImageMagick三方库说明
## 功能简介
 ImageMagick是一个免费的开源软件套件，用于显示、转换和编辑光栅图像和矢量图像文件。

## 三方库版本
- 7.1.1-35

## 已适配功能
- 支持图像格式转换、图像编辑（图像尺寸调整、旋转、锐化和减色）和图像添加水印

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)
