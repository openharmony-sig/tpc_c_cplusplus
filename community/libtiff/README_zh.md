# libtiff三方库说明
## 功能简介
libtiff是一个用来读写标签图片(tiff)的库。该库还支持如下文件格式的转化。

## 三方库版本
- 51558511bdbbcffdce534db21dbaf5d54b31638a

## 已适配功能
- 支持如下格式转换

  | 源文件格式 | 转化后格式 |
  | :--------: | :--------: |
  |    tiff    |    tiff    |
  |    pgm     |    tiff    |
  |     g3     |    tiff    |
  |    pbm     |    tiff    |
  |    ppm     |    tiff    |
  |    tiff    |    pdf     |
  |    tiff    |     ps     |

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [系统hap包集成](docs/hap_integrate.md)
