# json三方库说明
## 功能简介
json是一个C++的处理json数据解析的库。

## 三方库版本
- v3.11.2

## 已适配功能
- json数据解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
