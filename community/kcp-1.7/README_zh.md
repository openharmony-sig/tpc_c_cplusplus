# kcp-1.7三方库说明
## 功能简介
kcp是一个快速可靠的ARQ协议
## 三方库版本：
- 1.7
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
