# MQTT三方库说明
## 功能简介
MQTT 是用 C 语言编写的用于MQTT协议的Eclipse Paho C客户端库。

## 三方库版本
- paho.mqtt.c-v1.3.12

## 已适配功能
- 使用该库使应用程序能够连接到MQTT代理以发布消息，并订阅主题和接收已发布的消息。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
