# World-1.0.0三方库说明
## 功能简介
World是一个高质量的语音分析，操作和合成系统。
## 三方库版本：
- v1.0.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
