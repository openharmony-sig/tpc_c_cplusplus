# iproute2三方库说明
## 功能简介
iproute2是一个Linux操作系统中的工具集，用于网络栈的配置、监控和管理。它提供了一组命令行工具和一套API，用于处理数据包路由、网络设备、网络地址和协议。

## 三方库版本
- 6.4.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
