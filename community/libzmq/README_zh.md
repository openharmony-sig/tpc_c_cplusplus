# zeromq三方库说明
## 功能简介
ZeroMQ轻量级消息传递内核是一个库，它扩展了标准套接字接口，使用传统上由专门的消息传递中间件产品提供的功能。

## 三方库版本
- v4.8.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
