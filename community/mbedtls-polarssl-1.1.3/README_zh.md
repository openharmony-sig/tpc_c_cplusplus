# mbedtls-polarssl 三方库说明
## 功能简介
它是一个开源的、可移植的、易于使用的、可读的和灵活的TLS库，以及PSA加密API的参考实现。

## 三方库版本
- 1.1.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
