# utf8proc-2.7.0三方库说明
## 功能简介
utf8proc是一个紧凑、简洁而高效的C库，它为UTF-8编码的数据提供Unicode规范化、大小写转换等操作。
## 三方库版本：
- v2.7.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)