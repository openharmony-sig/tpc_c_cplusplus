# squirrel三方库说明
## 功能简介
Squirrel是一种轻量级的编程语言，通常用于游戏开发和应用程序的脚本编写，比C语言更简单，更易于嵌入到应用程序中。
## 三方库版本：
- v3.2
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)s