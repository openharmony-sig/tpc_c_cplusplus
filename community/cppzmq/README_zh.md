# cppzmq三方库说明
## 功能简介
cppzmq是一个基于ZeroMQ的开源C++库，用于构建分布式和并发应用程序。

## 三方库版本
- v4.8.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
