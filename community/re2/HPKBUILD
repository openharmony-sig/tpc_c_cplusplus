# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Contributor: wangjialun <2271411@stu.neu.edu.cn>, zhangqian<2371418@stu.neu.edu.cn>, wangyihao<2942375747@qq.com> , wangying<wangying@swc.neu.edu.cn>
# Maintainer: wangyihao<2942375747@qq.com>

pkgname=re2
pkgver=2022-12-01
pkgrel=0
pkgdesc="RE2 is a thread-friendly alternative to backtracking regular expression engines like those used in PCRE, Perl, and Python."
url="https://github.com/google/re2/tree/2022-12-01"
archs=("armeabi-v7a" "arm64-v8a")
license=("BSD-3-Clause license")
depends=()
makedepends=()
source="https://github.com/google/$pkgname/archive/refs/tags/$pkgver.zip"

downloadpackage=true
autounpack=true
buildtools=cmake

builddir=re2-2022-12-01
packagename=re2-2022-12-01.zip

prepare() {
    mkdir -p $builddir/$ARCH-build
}

build() {
    cd $builddir
    ${OHOS_SDK}/native/build-tools/cmake/bin/cmake "$@" -DCMAKE_C_FLAGS="-Wno-unused-command-line-argument" \
        -DCMAKE_CXX_FLAGS="-Wno-unused-command-line-argument" -B$ARCH-build -S./ > $buildlog 2>&1
    $MAKE VERBOSE=1 -C $ARCH-build >> $buildlog 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
}

package() {
    cd $builddir
    $MAKE -C $ARCH-build install >> $buildlog 2>&1
    cd $OLDPWD
}


check() {
    echo "The test must be on an OpenHarmony device!"
}


cleanbuild() {
    rm -rf ${PWD}/$builddir
}