# re2三方库说明
## 功能简介
re2是一种快速、安全、线程友好的c++库，可以回溯PCRE、Perl和Python中使用的正则表达式引擎。
## 三方库版本：
- 2022-12-01
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)