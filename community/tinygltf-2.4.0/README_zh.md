# tinygltf-2.4.0三方库说明
## 功能简介
tinygltf是一个用于处理glTF文件的C++头文件库，用于在不同的应用程序和平台之间高效地处理和交换3D内容。
## 三方库版本：
- v2.4.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)