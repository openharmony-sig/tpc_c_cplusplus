# libiec61850三方库说明
## 功能简介
libiec61850是IEC 61850客户端和服务器库的开源(GPLv3)实现，实现了MMS、GOOSE和SV协议。

## 三方库版本
- v1.1.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
