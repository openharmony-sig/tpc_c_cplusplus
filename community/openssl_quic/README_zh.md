# openssl_quic三方库说明
## 功能简介
openssl_quic是openssl加密库的一个分支用于启用quic。

## 三方库版本
- OpenSSL_1_1_1t-quic1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
