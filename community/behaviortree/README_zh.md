# behaviortree三方库说明

## 功能简介
behaviortree提供了一个创建行为树的框架，常用来做任务或状态管理。

## 三方库版本
- 4.1.1

## 已适配功能
- behaviortree行为树任务或状态管理功能

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](./docs/hap_integrate.md)