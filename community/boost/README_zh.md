# boost三方库说明
## 功能简介
Boost provides free peer-reviewed portable C++ source libraries。

## 三方库版本
- 1.81.0

## 已适配功能
- 除去log,python,locale的其他功能都适配

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)
