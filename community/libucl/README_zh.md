# libucl三方库说明
## 功能简介
libucl是一种通用的配置库解析器，它与JSON语言兼容，因此可以用作简单的JSON解析器。

## 三方库版本
- 0.8.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
