# mpfr 三方库说明
## 功能简介
mpfr库是一个C库，用于正确舍入的多精度浮点计算。

## 三方库版本
- 4.2.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
