# json-c三方库说明
## 功能简介
json-c是json数据解析库。

## 三方库版本
- 0.17-20230812

## 已适配功能
- 支持json数据解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
