# Catch2三方库说明
## 功能简介
Catch2主要是c++的单元测试框架，但它也提供了基本的微基准测试特性和简单的BDD宏。

## 三方库版本
- v2.13.8

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
