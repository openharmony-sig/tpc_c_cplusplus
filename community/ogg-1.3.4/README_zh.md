# ogg三方库说明
## 功能简介
Ogg项目编解码器使用Ogg比特流格式可以将原始的压缩比特流排列成更健壮、更有用的形式。
## 三方库版本：
- 1.3.4
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
