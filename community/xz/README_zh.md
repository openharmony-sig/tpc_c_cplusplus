# xz 三方库说明

## 功能简介

XZ 是免费的通用数据压缩软件，具有较高的压缩比。

## 三方库版本
- 5.2.6

## 已适配功能
- 完成了 .lzma 格式文件的压缩、解压缩

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
