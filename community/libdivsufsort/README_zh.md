# libdivsufsort三方库说明
## 功能简介
libdivsufsort是一个轻量级的后缀排序库。
## 三方库版本：
- 2.0.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)