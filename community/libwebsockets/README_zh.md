# libwebsockets 三方库说明
## 功能简介
libwebsockets是一款轻量级用来开发服务器和客户端的C库。
## 三方库版本
- v4.3.3
## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)