# linux-pam 三方库说明
## 功能简介
Linux-PAM是一套允许本地系统管理员随意选择程序认证方式的共享库。它通过提供一套灵活的验证机制，使得系统管理员可以在不重新编译应用程序的情况下，轻松切换或升级认证机制。

## 三方库版本
- 8562cb1b951e7bd807af6b43d85c71cedd7b10d7

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
