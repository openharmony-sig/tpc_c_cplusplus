# minizip-ng三方库说明

## 功能简介

minizip-ng是一个用C编写的zip操作库，支持Windows、macOS和Linux。。

## 三方库版本
- 1.2（8658af7e0a73d6ac4d94e81dde0e9fa95b1aff47）

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

+ [应用hap包集成](docs/hap_integrate.md)
