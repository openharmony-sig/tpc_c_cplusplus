# zlib 三方库说明
## 功能简介
zlib 是一个通用的数据压缩库。所有代码都是线程安全的。

## 三方库版本
- v1.3.1

## 已适配功能
- 数据压缩

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
