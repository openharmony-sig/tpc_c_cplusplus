# libsamplerate-0.2.2三方库说明
## 功能简介
libsamplerate是一个用于执行音频数据采样率转换的库。
## 三方库版本：
- 0.2.2
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)