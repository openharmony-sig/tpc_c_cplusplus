# draco三方库说明
## 功能简介
draco用于改善3D图形的存储和传输。

## 三方库版本
- v1.5.4

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
