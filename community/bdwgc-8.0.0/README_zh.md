# bdwgc-8.0.0三方库说明
## 功能简介
bdwgc-8.0.0是一个面向C和c++的保守垃圾收集器
## 三方库版本：
- v8.0.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
