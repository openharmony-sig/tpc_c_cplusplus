# CUnit三方库说明
## 功能简介
CUnit是C语言的单元测试框架

## 三方库版本
- 2.1-3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
