# capstone三方库说明
## 功能简介
Capstone是一个支持多种硬件架构的反汇编框架。
## 三方库版本：
- 4.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)