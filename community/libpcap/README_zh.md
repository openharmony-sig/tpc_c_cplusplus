# libpcap三方库说明
## 功能简介
libpcap是unix/linux平台下的网络数据包捕获函数包，大多数网络监控软件都以它为基础。

## 三方库版本
- 1.10.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
