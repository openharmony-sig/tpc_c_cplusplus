# fmt 三方库说明
## 功能简介
fmt是一个开源格式库，可提供C stdio和C ++ iostreams的快速安全替代品。

## 三方库版本
- 8.1.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
