# 应用集成到hap的开发环境准备

## 环境搭建
- 使用ubuntu20.04及以上系统，推荐使用ubuntu20.04
- [SDK准备Command Line Tools Beta6及以上版本（api12）](https://developer.huawei.com/consumer/cn/download)
- [IDE准备DevEco Studio NEXT Developer Beta6及以上版本](https://developer.huawei.com/consumer/cn/download)
- [准备三方库构建环境](../lycium/README.md#1编译环境准备)
- [准备三方库测试环境](../lycium/README.md#3ci环境准备)

