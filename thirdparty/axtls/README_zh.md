# axtls 三方库说明
## 功能简介
axTLS 嵌入式 SSL 项目是一个高度可配置的客户端/服务器 TLSv1.2 库，专为内存需求较小的平台而设计。它带有一个小型HTTP / HTTPS服务器和其他测试工具。

## 三方库版本
- 2.1.5

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
