# ngtcp2三方库说明
## 功能简介
ngtcp2项目是实现RFC9000 QUIC协议。

## 三方库版本
- 3.3.10

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [系统hap包集成](docs/hap_integrate.md)
