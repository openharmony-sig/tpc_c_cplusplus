# xxHash 三方库说明
## 功能简介
xxHash是一种极快的哈希算法，在RAM速度限制下处理。 代码具有高度可移植性

## 三方库版本
- v0.8.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
