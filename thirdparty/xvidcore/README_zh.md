# xvidcore 三方库说明
## 功能简介
xvidcore  是一个开源的 MPEG-4 Part 2 编解码器，被广泛用于创建和播放视频内容。

## 三方库版本
- 1_3_7

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)

