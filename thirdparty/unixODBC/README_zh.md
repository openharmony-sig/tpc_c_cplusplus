# unixODBC三方库说明
## 功能简介
unixODBC项目的目标是开发和推广unixODBC，使其成为非MS Windows平台上ODBC的最终标准。

## 三方库版本
- 2.3.11

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
