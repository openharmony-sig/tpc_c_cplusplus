# libiscsi三方库说明
## 功能简介
libiscsi是一个客户端库，用于实现iSCSI协议，可用于访问iSCSI目标的资源

## 三方库版本
- 1.19.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)

