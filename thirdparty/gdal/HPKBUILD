# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Contributor: baijn <1225837220@qq.com>
# Maintainer: baijn <1225837220@qq.com>

pkgname=gdal
pkgver=v3.9.1
pkgrel=0
pkgdesc="GDAL is an translator library for raster and vector geospatial data formats."
url="https://github.com/OSGeo/gdal"
archs=("armeabi-v7a" "arm64-v8a")
license=("MIT")
depends=("PROJ" "geos")
makedepends=()
source="https://github.com/OSGeo/$pkgname/archive/refs/tags/$pkgver.tar.gz"

downloadpackage=true
autounpack=true
buildtools="cmake"
builddir=$pkgname-${pkgver:1}
packagename=$builddir.tar.gz
patchflag=true

prepare() {
    mkdir -p $builddir/$ARCH-build
    if ${patchflag}
    then
        cd ${builddir}

        # 更改写入测试目录 /tmp 为 /data/local/tmp
        patch -p1 < ../${pkgname}_oh_test.patch
        echo "patching success"
        patchflag=false
        cd ${OLDPWD}
    fi
}

build() {
    # 需设置该环境变量，将路径写入测试用例CTestTestfile.cmake文件中
    export LD_LIBRARY_PATH="`pwd`/$builddir/$ARCH-build/lib/:${LYCIUM_ROOT}/usr/gdal/$ARCH/lib/:${LYCIUM_ROOT}/usr/zlib_1_3_1/$ARCH/lib/:${LYCIUM_ROOT}/usr/PROJ/$ARCH/lib/:${LYCIUM_ROOT}/usr/sqlite_3_46_0/$ARCH/lib/:${LYCIUM_ROOT}/usr/libtiff/$ARCH/lib/:${LYCIUM_ROOT}/usr/curl_8_9_1/$ARCH/lib/:${LYCIUM_ROOT}/usr/libdeflate/$ARCH/lib/:${LYCIUM_ROOT}/usr/libjpeg-turbo/$ARCH/lib/:${LYCIUM_ROOT}/usr/xz/$ARCH/lib/:${LYCIUM_ROOT}/usr/zstd_1_5_6/$ARCH/lib/:${LYCIUM_ROOT}/usr/libwebp/$ARCH/lib/:${LYCIUM_ROOT}/usr/nghttp2/$ARCH/lib/:${LYCIUM_ROOT}/usr/openssl_1_1_1w/$ARCH/lib"
    cd $builddir
    ${OHOS_SDK}/native/build-tools/cmake/bin/cmake "$@" -B$ARCH-build -S./ > $buildlog 2>&1
    $MAKE -C $ARCH-build  >> $buildlog 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
}

package() {
    cd $builddir
    $MAKE -C $ARCH-build install >> $buildlog 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
}

check() {
    echo "The test must be on an OpenHarmony device!"
}

cleanbuild() {
    rm -rf ${PWD}/$builddir #${PWD}/$packagename
}
