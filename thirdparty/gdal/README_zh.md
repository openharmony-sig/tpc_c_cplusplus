# gdal三方库说明
## 功能简介
  GDAL（Geospatial Data Abstraction Library）是一个在X/MIT许可协议下的栅格和矢量地理空间数据格式转换库。

## 三方库版本
- v3.9.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)