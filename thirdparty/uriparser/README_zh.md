# uriparser 三方库说明

## 功能简介

uriparser 是一个开源的 URI（统一资源标识符）解析库，用于解析和处理 URI。

## 三方库版本
- 0.9.8

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

+ [应用hap包集成](docs/hap_integrate.md)
