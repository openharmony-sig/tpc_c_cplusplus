# xmlrpc-c 三方库说明
## 功能简介
XML-RPC is a quick-and-easy way to make procedure calls over the Internet. It converts the procedure call into an XML document, sends it to a remote server using HTTP, and gets back the response as XML.

## 三方库版本
- 1.54.06 

## 已适配功能
- 通过xml格式进行，RPC通信

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
