# libyaml三方库说明
## 功能简介
libyaml是一个解析和发出YAML的C库。

## 三方库版本
- 0.2.5

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
