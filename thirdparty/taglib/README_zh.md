# taglib三方库说明
## 功能简介
TagLib是一个用于读取和编辑几种流行音频格式的元数据的库.

## 三方库版本
- v1.13.1

## 已适配功能
- 音频元数据格式编辑

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)