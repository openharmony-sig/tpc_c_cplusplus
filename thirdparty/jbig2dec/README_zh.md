# jbig2dec三方库说明
## 功能简介
jbig2dec是JBIG2图像压缩格式的解码器实现。

## 三方库版本
- 0.19

## 已适配功能
- JBIG2图像压缩格式的解码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
