# libmediasoupclient三方库说明
## 功能简介
libmediasoupclient是一个C++客户端库，用于构建基于 mediasoup 的应用程序。

## 三方库版本
- 3.4.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)