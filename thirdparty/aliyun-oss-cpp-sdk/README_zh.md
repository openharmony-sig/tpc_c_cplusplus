# aliyun-oss-cpp-sdk-1.10.0三方库说明
## 功能简介
aliyun-oss-cpp-sdk-1.10.0是阿里云对外提供的海量、安全、低成本、高可靠的云存储服务。
## 三方库版本：
- 1.10.0
## 使用约束：
- [IDE和SDK版本](../../docs/constraint.md)
## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
