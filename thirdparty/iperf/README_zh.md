# iperf三方库说明
## 功能简介
iperf是一个网络性能测试工具，iperf可以测试TCP和UDP带宽质量。iperf即可测量最大TCP带宽，也具有多种参数和UDP特性，且可报告带宽，延迟抖动和数据包丢失

## 三方库版本
- 3.14

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
