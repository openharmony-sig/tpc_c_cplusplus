# libtomcrypt 三方库说明
## 功能简介
libtomcrypt 是一个模块化的、可移植的、免费的开源加密库，它提供了各种加密算法的实现，包括对称加密算法（如 AES、DES）、公钥加密算法（如 RSA、ECC）、哈希算法（如 SHA-256、MD5）等。

## 三方库版本
- 2302a3a89752b317d59e9cdb67d2d4eb9b53be8e

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
