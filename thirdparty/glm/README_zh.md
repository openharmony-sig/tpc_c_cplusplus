# glm三方库说明
## 功能简介
OpenGL Mathematics（GLM）是一个基于OpenGL着色语言（GLSL）规范的图形软件的仅限标题的C++数学库。

## 三方库版本
- 0.9.9.8

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)