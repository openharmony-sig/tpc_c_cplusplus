# pugixml三方库说明

## 功能简介

pugixml是一个C++XML处理库。

## 三方库版本
- v1.13

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
