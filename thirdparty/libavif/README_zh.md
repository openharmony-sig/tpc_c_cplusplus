# libavif三方库说明
## 功能简介
libavif用于编码和解码avif格式图像文件

## 三方库版本
- 1.0.1

## 已适配功能
- 支持avif格式图像文件

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)

