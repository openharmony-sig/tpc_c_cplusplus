# sqlite3pp三方库说明
## 功能简介
sqlite3pp是一个提供数据库操作能力的库。

## 三方库版本
- v1.0.9

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
