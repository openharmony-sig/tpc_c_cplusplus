# faad2三方库说明
## 功能简介
faad2是一个音频解码的库。

## 三方库版本
- 2.10.1

## 已适配功能
- 支持音频的解码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
