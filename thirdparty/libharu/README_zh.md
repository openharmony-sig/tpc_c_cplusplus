# libharu三方库说明
## 功能简介
libharu是一款免费、跨平台、开源的，用于生成PDF的库。

## 三方库版本
- v2.4.4

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
