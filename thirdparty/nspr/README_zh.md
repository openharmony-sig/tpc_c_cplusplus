# nspr 三方库说明
## 功能简介
Netscape Portable Runtime（NSPR）为系统级和类似libc的函数提供了一个平台-中间API。API用于Mozilla客户端、许多Red Hat和Oracle的服务器应用程序以及其他软件产品。

## 三方库版本
- 4.35

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
