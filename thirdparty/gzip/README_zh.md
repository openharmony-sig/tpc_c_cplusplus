# gzip三方库说明
## 功能简介
  gzip是一个压缩实例程序，旨在替代compress，与压缩相比，它的主要优点是压缩效果更好并且不受专利算法的约束。

## 三方库版本
- 1.13

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)