# libvpx三方库说明
## 功能简介
libvpx是支持vp8、vp9编码解码的开源软件

## 三方库版本
- 1.13.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
