# sentencepiece三方库说明

## 功能简介

sentencepiece用于基于神经网络的文本生成的无监督文本标记器。

## 三方库版本
- 0.2.0

## 已适配功能
- 提供生成训练模型文件、编解码功能和格式化功能。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用包hap集成](docs/hap_integrate.md)
