# marisa-trie 三方库说明

## 功能简介

marisa-trie 递归存储匹配算法种静态的、节省空间的trie数据结构.

## 三方库版本
- v0.2.6

## 已适配功能
- 提供字典树数据结构算法，用于生成并加载asr字典树.

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
