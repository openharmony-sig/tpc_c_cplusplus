# Contributor: lpzhong <278527840@qq.com>
# Maintainer: lpzhong <278527840@qq.com>

pkgname=marisa-trie
pkgver=v0.2.6
pkgrel=0
pkgdesc="Matching Algorithm with Recursively Implemented StorAge (MARISA) is a static and space-efficient trie data structure."
url="https://github.com/s-yata/marisa-trie"
archs=("armeabi-v7a" "arm64-v8a")
license=("BSD-2-Clause" "LGPL-2.1-or-later")
depends=()
makedepends=()
install=
source="https://github.com/s-yata/${pkgname}/archive/refs/tags/${pkgver}.tar.gz"

autounpack=true
downloadpackage=true
buildtools="configure"

builddir=$pkgname-${pkgver:1}
packagename=$builddir.tar.gz

source envset.sh
host=
prepare() {
    cp -arf $builddir $builddir-$ARCH-build
    if [ $ARCH == "armeabi-v7a" ]; then
        setarm32ENV
        host=arm-linux
        export LDFLAGS="${OHOS_SDK}/native/llvm/lib/clang/${CLANG_VERSION}/lib/arm-linux-ohos/a7_hard_neon-vfpv4/libclang_rt.builtins.a ${LDFLAGS}"
    fi
    if [ $ARCH == "arm64-v8a" ]; then
        setarm64ENV
        host=aarch64-linux
        export LDFLAGS="${OHOS_SDK}/native/llvm/lib/clang/${CLANG_VERSION}/lib/aarch64-linux-ohos/libclang_rt.builtins.a ${LDFLAGS}"
    fi
    cd $builddir-$ARCH-build
    autoreconf -i > $publicbuildlog 2>&1
    cd $OLDPWD
}

build() {
    cd $builddir-$ARCH-build
    PKG_CONFIG_LIBDIR="${pkgconfigpath}" ./configure "$@" --host=$host > $buildlog 2>&1
    $MAKE VERBOSE=1 >> $buildlog 2>&1
    ret=$?

    cd $OLDPWD
    return $ret
}

package() {
    cd $builddir-$ARCH-build
    $MAKE install >> $buildlog 2>&1
    ret=$?
    cd $OLDPWD
    return $ret
}

check() {
    cd $builddir-$ARCH-build/tests
    ## 编译出测试应用
    $MAKE base-test io-test vector-test trie-test marisa-test >> $buildlog 2>&1
    ret=$?
    ## 修改测试条件， 使测试时不在进行编译操作
    sed -i "s/check-TESTS: \$(check_PROGRAMS)/check-TESTS: # \$(check_PROGRAMS)/" Makefile
    sed -i "s/base-test.log: base-test\$(EXEEXT)/base-test.log: # base-test\$(EXEEXT)/" Makefile
    sed -i "s/io-test.log: io-test\$(EXEEXT)/io-test.log: # io-test\$(EXEEXT)/" Makefile
    sed -i "s/vector-test.log: vector-test\$(EXEEXT)/vector-test.log: # vector-test\$(EXEEXT)/" Makefile
    sed -i "s/trie-test.log: trie-test\$(EXEEXT)/trie-test.log: # trie-test\$(EXEEXT)/" Makefile
    sed -i "s/marisa-test.log: marisa-test\$(EXEEXT)/marisa-test.log: # marisa-test\$(EXEEXT)/" Makefile

    cd $OLDPWD

    if [ $ARCH == "armeabi-v7a" ]; then
        unsetarm32ENV
    fi
    if [ $ARCH == "arm64-v8a" ]; then
        unsetarm64ENV
    fi

    unset host
    echo "The test must be on an OpenHarmony device!"

    ## real test cmd
    ## make check-TESTS -C tests

    return $ret
}

# 清理环境
cleanbuild(){
    rm -rf ${PWD}/$builddir #${PWD}/$packagename
    rm -rf ${PWD}/*-build
}
