# c-ares三方库说明
## 功能简介
c-ares是一个C语言的异步DNS解析库.

## 三方库版本
- cares-1_19_0

## 已适配功能
- 提供异步DNS解析能力

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
