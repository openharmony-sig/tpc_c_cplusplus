# luv三方库说明
## 功能简介
luv是一个用于lua的libuv裸绑定的库

## 三方库版本
- 1.45.0-0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)