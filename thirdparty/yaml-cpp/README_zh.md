# yaml-cpp三方库说明
## 功能简介
yaml-cpp是c++中的YAML解析器和发射器，与YAML 1.2规范相匹配。

## 三方库版本
- v0.8.0

## 已适配功能
- 提供解析和生成YAML的能力。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
