# libde265三方库说明
## 功能简介
libde265用于h265格式视频编解码

## 三方库版本
- v1.0.15

## 已适配功能
- 支持h265格式的视频解码，暂不支持编码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
