# variant2三方库说明
## 功能简介
variant2 library implements a type-safe discriminated/tagged union type.

## 三方库版本
- 1.81.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](docs/hap_integrate.md)
