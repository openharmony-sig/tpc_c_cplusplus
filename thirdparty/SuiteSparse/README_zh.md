# SuiteSparse 三方库说明
## 功能简介
SuiteSparse是一款优秀的稀疏矩阵计算工具包，它提供了丰富的算法和函数来处理稀疏矩阵的各类问题。

## 三方库版本
- v7.7.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
