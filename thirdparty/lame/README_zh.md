# lame三方库说明
## 功能简介
lame是非常优秀的一种MP3编码器。

## 三方库版本
- 3.100

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
