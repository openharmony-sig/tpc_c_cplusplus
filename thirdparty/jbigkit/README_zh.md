# jbigkit三方库说明
## 功能简介
JBIG-KIT是JBIG1数据压缩标准（ITU-T T.82）的软件实现，该标准是为扫描文档等双层图像数据设计的。

## 三方库版本
- v2.5.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
