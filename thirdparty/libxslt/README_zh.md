# libxslt三方库说明
## 功能简介
Libxslt库用于处理和转换XML文档。

## 三方库版本
- 1.1.38

## 已适配功能
- 允许通过使用XSLT样式表来转换XML文档的结构和内容。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [系统hap包集成](docs/hap_integrate.md)
