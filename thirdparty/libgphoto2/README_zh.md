# libgphoto2三方库说明
## 功能简介
libgphoto2是一个功能强大的库，用于通过应用程序访问和控制数字相机。

## 三方库版本
- v2.5.31

## 已适配功能
- 支持多种相机的访问和设置，图像捕获和控制，文件传输，延迟拍摄和定时器

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)
