# speechd 三方库说明
## 功能简介
Speech Dispatcher项目提供了一个高级的独立于设备的层，用于通过一个简单、稳定且文档良好的接口访问语音合成。

## 三方库版本
- 0.11.5

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
