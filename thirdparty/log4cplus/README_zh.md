# log4cplus三方库说明

## 功能简介

log4cplus是一个简单易用的C++日志记录API，它对日志管理和配置提供了线程安全、灵活和任意粒度的控制.

## 三方库版本
- REL_2_1_0

## 已适配功能
- 支持输出日志并将日志保存到本地

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
