# libmng 三方库说明
## 功能简介
libmng 是用于阅读，显示，写作和检查多图像网络图形的参考库。

## 三方库版本
- 2.0.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
