# soxr三方库说明
## 功能简介
soxr是一个重采样库，执行一维采样率转换，例如，可用于对 PCM 编码的音频进行重采样。

## 三方库版本
- 0.1.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
