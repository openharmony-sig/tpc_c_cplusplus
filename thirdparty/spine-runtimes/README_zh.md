# spine-runtimes 三方库说明
## 功能简介
spine-runtimes项目托管了Spine运行时。

## 三方库版本
- 4.1.00

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
