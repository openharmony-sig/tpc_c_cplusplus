# zxing-cpp三方库说明
## 功能简介
zxing-cpp是一个二维码生成和解析的库。

## 三方库版本
- v2.0.0

## 已适配功能
- 二维码生成和解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
