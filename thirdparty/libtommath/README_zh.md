# libtommath三方库说明
## 功能简介
libtommath是一个完全用C语言编写的免费开源可移植数字理论多精度整数（MPI）库。

## 三方库版本
- 1.2.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
