# liblinear三方库说明
## 功能简介
liblinear 是一个用于解决线性规划、回归算法和异常检测的库。

## 三方库版本
- 2.4.6

## 已适配功能
- 提供线性规划、SVM、异常检测等。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)