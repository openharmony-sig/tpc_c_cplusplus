# aubio 三方库说明
## 功能简介
aubio是一个标记音乐和声音的库。它监听音频信号并尝试检测事件。例如，当击鼓时，什么频率是一个音符，或者什么速度是有节奏的旋律。

## 三方库版本
- aubio-152d6819b360c2e7b379ee3f373d444ab3df0895

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
