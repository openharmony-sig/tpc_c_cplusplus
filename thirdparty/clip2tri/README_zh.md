# clip2tri三方库说明
## 功能简介

 clip2tri是使用clipper和poly2tri一起进行鲁棒三角剖分的三方库。

## 三方库版本
- clip2tri-f62a734d22733814b8a970ed8a68a4d94c24fa5f

## 已适配功能
- 支持生成导航网格区域，使用浮点数据作为输入。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)



