# djvulibre三方库说明
## 功能简介
djvulibre是一组压缩技术，一种文件格式，以及用于通过网络递送数字文档的软件平台，扫描文档和高分辨率图像。

## 三方库版本
- 3.5.27.1-7

## 已适配功能
- 支持把pbm转换djuv以及多个djuv文件合并

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
