# p7zip三方库说明
## 功能简介
p7zip 是一个功能齐全的压缩打包应用。

## 三方库版本
- v17.05

## 已适配功能
- 文件压缩，解压，打包，解包

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
