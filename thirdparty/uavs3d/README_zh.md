# uavs3d三方库说明
## 功能简介
uavs3d是一个开源和跨平台的avs3解码器

## 三方库版本
- 1fd04917cff50fac72ae23e45f82ca6fd9130bd8

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)
