# libkate三方库说明
## 功能简介
- libkate是一个用于处理Kate流元数据的开源库

## 三方库版本
- libkate-distrotech-libkate-0.4.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
