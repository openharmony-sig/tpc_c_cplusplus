# opus三方库说明

## 功能简介

Opus是一个用于在互联网上进行交互式语音和音频传输的编解码器。

## 三方库版本
- 1.4

## 已适配功能
- 支持语音和音乐
- 支持单声道和立体声
- 支持采样率8khz到48khz

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
 [应用包hap集成](docs/hap_integrate.md)





