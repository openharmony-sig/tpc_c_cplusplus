# libzip三方库说明
## 功能简介
libzip是一个用于读取、创建和修改zip存档的C库。文件可以从数据缓冲区、文件或直接从其他zip档案复制的压缩数据中添加。

## 三方库版本
- 1.9.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
