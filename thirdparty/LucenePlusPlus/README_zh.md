# LucenePlusPlus三方库说明
## 功能简介
Lucene++ 是 Java Lucene 库的一个 C++ 端口，Java Lucene 是一个高性能、功能齐全的文本搜索引擎。

## 三方库版本
- rel_3.0.9

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
