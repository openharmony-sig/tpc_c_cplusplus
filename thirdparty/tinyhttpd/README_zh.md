# tinyhttpd 三方库说明
## 功能简介
ttpd是一个简单、小型、可移植、快速且安全的HTTP服务器。

## 三方库版本
- b35e7daf10426f9be1e22ddc0ba8c6d23225c817

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
