# openal-soft三方库说明
## 功能简介
OpenAL Soft是OpenAL 3D音频API的软件实现。

## 三方库版本
- 1.23.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
