# ncnn三方库说明
## 功能简介
NCNN 是一个高性能的神经网络推理框架。

## 三方库版本
- 20240102

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
