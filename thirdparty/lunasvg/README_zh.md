# lunasvg 三方库说明
## 功能简介
lunasvg 用于创建，设置动画，操作和渲染SVG文件

## 三方库版本
- 2.3.9

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
