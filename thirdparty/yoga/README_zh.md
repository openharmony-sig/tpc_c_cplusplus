# yoga三方库说明
## 功能简介
libyoga是一个UI控件布局引擎。

## 三方库版本
- v3.0.2

## 已适配功能
- 提供UI控件布局能力。此库只适配C层，不提供TS接口的能力

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
