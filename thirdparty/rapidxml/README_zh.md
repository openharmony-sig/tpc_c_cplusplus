# rapidxml三方库说明
## 功能简介
  RapidXML 是一个用于解析和操作 XML 文档的 C++ 库。它提供了一组简单而高效的 API，可以快速解析和访问 XML 数据。RapidXML 以其原位解析技术而闻名，这使得它在解析速度上表现出色，接近 strlen 函数在同一数据上的执行速度。

## 三方库版本
- 9872a2fae2912697bd9f3a3f499485f3afca6cc0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)