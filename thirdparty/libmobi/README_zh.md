# libmobi三方库说明
## 功能简介
libmobi是一个用于处理Mobipocket/Kindle（MOBI）电子书格式文档的C库

## 三方库版本
- v0.11

## 已适配功能
- MOBI格式解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
