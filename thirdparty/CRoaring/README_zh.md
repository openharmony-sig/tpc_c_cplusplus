# CRoaring三方库说明
## 功能简介
CRoaring是一款位图（bitmap or bitset）管理软件，可用于数据库或搜索引擎场景中。

## 三方库版本
- 3.0.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
