# boringssl三方库说明
## 功能简介
BoringSSL是OpenSSL的一个分支，旨在满足Google的需求。

## 三方库版本
- fips-20220613

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
