# tink三方库说明
## 功能简介
tink一个多语言、跨平台的库，提供安全、易于正确使用且难以滥用的加密API。

## 三方库版本
- v1.7.0

## 已适配功能
- 安全加密

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)
