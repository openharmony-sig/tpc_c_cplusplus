# pupnp三方库说明
## 功能简介
pupnp是提供UPnP协议能力的三方库。

## 三方库版本
- v1.0.12

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
