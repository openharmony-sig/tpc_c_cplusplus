# LuaXML三方库说明
## 功能简介
LuaXML作为一个开源项目，是在Lua和XML之间建立映射的模块。

## 三方库版本
- 7cd4a7ab5db85222edb4c955d53e5674afd170b6

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
