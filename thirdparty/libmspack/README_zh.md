# libmspack 三方库说明
## 功能简介
libmspack 是一个开源库，用于解压缩 MSPACK 格式的文件，这种格式通常用于 CAB（Cabinet）文件的压缩。

## 三方库版本
- v1.11

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
