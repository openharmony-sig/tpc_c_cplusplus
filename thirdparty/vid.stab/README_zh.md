# vid.stab三方库说明
## 功能简介
vid.stab是一个处理视频抖动的库。

## 三方库版本
- v1.1.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)
