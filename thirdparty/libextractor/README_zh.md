# libextractor 三方库说明
## 功能简介
libextractor是一个用于从文件中提取元数据的库。

## 三方库版本
- v1.11

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
