# qhull 三方库说明

## 功能简介

qhull 是一个用于计算凸包、Delaunay 三角剖分和 Voronoi 图等计算几何问题的开源库。

## 三方库版本
- v8.1-alpha3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

+ [应用hap包集成](docs/hap_integrate.md)
