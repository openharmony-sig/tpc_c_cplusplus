# RingBuffer 三方库说明
## 功能简介
一个简单易用的环形缓冲库。

## 三方库版本
- 1.0.4

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
