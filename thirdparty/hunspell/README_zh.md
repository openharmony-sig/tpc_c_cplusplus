# hunspell 三方库说明

## 功能简介
hunspell 是一个免费的拼写检查器和形态分析器库和命令行工具.

## 三方库版本
- v1.7.2

## 已适配功能
- 支持拼写检查以及形态分析。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
