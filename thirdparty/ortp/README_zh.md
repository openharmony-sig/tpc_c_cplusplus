# ortp三方库说明
## 功能简介
ortp是一个实现RTP协议的C库

## 三方库版本
- 5.2.89

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
