# bcunit三方库说明
## 功能简介
bcunit是一个单元测试框架。

## 三方库版本
- 5.2.62

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
