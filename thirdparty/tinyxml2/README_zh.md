# tinyxml2三方库说明
## 功能简介
TinyXML2 是 simple、small、efficient 的开源 C++ XML 文件解析库，可以很方便地应用到现有的项目中。

## 三方库版本
- v9.0.0

## 已适配功能
- xml解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用Hap包集成](docs/hap_integrate.md)
