# Chipmunk2D三方库说明
## 功能简介
Chipmunk2D是一个在MIT许可下分发的2D刚体物理库。

## 三方库版本
- 7.0.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
