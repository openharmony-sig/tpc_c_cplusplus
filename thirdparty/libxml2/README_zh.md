# libxml2三方库说明
## 功能简介
libxml2是一个用于解析XML文档的C语言库。它提供了一组API，可以用于读取、修改和创建XML文档

## 三方库版本
- v2.11.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
