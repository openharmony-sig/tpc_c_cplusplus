# libhevc三方库说明
## 功能简介
libhevc是一个格式编解码库

## 三方库版本
- simpleperf-release

## 已适配功能
- 支持h265格式的视频解码，yuv格式的编码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
