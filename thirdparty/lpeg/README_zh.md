# lpeg三方库说明
## 功能简介
LPEG是一个供lua使用的基于 Parsing Expression Grammars 的模式匹配库

## 三方库版本
- fda374f3bbac50653bdeba7404d63e13972e6210

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)