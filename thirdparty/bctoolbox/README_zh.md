# bctoolbox三方库说明
## 功能简介
通信软件使用的一些实用库，像belle-sip、mediastreamer2和liblinphone。

## 三方库版本
- 5.2.91

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)