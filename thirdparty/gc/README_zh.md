# gc 三方库说明
## 功能简介
gc是一种保守的、线程本地的、标记-清除垃圾收集器的实现。该实现为标准POSIX的malloc()、calloc()、realloc()和free()调用提供了一个功能齐全的替代。

## 三方库版本
- 7f6f17c8b3425df6cd27d6f9385265b23034a793

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
