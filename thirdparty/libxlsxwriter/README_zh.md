# libxlsxwriter三方库说明
## 功能简介
libxlsxwriter是一个可以向Excel写入文字和图片的库。

## 三方库版本
- RELEASE_1.1.5

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
