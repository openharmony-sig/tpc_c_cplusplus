# poly1305 三方库说明
## 功能简介
Poly1305 是一种广泛使用的 MAC 算法，特别适合于现代密码学中的加密套件。

## 三方库版本
- e6ad6e091d30d7f4ec2d4f978be1fcfcbce72781

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
