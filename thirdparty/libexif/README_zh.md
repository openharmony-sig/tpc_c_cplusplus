# libexif三方库说明
## 功能简介
libexif是一个用于解析、编辑和保存EXIF数据的库

## 三方库版本
- 0.6.24

## 已适配功能
- 从照片中读取相机快门速度，光圈，GPS定位和相机主人的姓名等照片信息，并且能解析和保存EXIF数据

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)