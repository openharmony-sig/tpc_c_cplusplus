# bisdiff 三方库说明

## 功能简介
bsdiff是一个提供二进制文件拆分以及合并能力的三方库。

## 三方库版本
- b817e9491cf7b8699c8462ef9e2657ca4ccd7667

## 已适配功能
- 实现文件的拆分以及合并功能。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](docs/hap_integrate.md)
