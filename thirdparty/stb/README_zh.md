# stb三方库说明
## 功能简介
stb是一个图像读写库

## 三方库版本
- 5736b15f7ea0ffb08dd38af21067c314d6a3aae9

## 已适配功能
- 图像加载、写出、缩放等功能

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)