# pvmp3dec三方库说明
## 功能简介
pvmp3dec是一个开源的MP3解码库

## 三方库版本
- 36ec11a

## 已适配功能
- 把MP3音频文件格式转换成PCM格式

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
