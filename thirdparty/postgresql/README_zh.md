# postgresql三方库说明

## 功能简介

PostgreSQL是一个功能强大的开源对象关系型数据库系统。

## 三方库版本
- 16.3

## 已适配功能
- 支持acid事物特性,确保数据的完整和一致性支持全文搜索
- 支持并发控制

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
 [应用包hap集成](docs/hap_integrate.md)





