# filesystem三方库说明
## 功能简介
filesystem是一个C++库，允许操作文件目录。

## 三方库版本
- 1.5.14

## 已适配功能
- 提供操作文件目录能力

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
