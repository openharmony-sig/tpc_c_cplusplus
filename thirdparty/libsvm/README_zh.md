# libsvm三方库说明
## 功能简介
libsvm是一个支持向量机的库。

## 三方库版本
- v331

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
