# cups三方库说明
## 功能简介
 cups是苹果公司为macOS®和其他类似UNIX®的操作系统开发的基于标准的开源打印系统。

## 三方库版本
- cups-2.3.6

## 已适配功能
- 支持系统打印机设备枚举、支持打印机设备控制、共享打印服务

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)



