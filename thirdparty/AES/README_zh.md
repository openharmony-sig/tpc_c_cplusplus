# AES三方库说明
## 功能简介
AES算法的C语言实现

## 三方库版本
- 44407f634f11992404596129ec31545e3f268ad3

## 已适配功能
- AES算法

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
