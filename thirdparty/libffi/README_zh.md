# libffi三方库说明
## 功能简介
libffi是一个开源库，提供了一种通用的调用外部函数的机制。       

## 三方库版本
- 3.4.4

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [系统hap包集成](docs/hap_integrate.md)
