# soci三方库说明

## 功能简介

soci是一个用于C++的数据库访问库，它提供了一个统一的接口来访问各种数据库系统，如MySQL、PostgreSQL、SQLite等。

## 三方库版本
- 884808c294e4809064650f3e5aed9102d8a68d56

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

+ [应用hap包集成](docs/hap_integrate.md)
