# Paddle-Lite三方库说明
## 功能简介
Paddle-Lite 是一个高性能的深度学习引擎，支持移动端和边缘设备。

## 三方库版本
- v2.12

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)