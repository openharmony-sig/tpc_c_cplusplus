# tremolo三方库说明
## 功能简介
Tremolo是xiph.org上Tremor lib的ARM优化版本。Tremor库是一个用于执行Ogg Vorbis解压缩的纯整数库。

## 三方库版本
- 0.08

## 已适配功能
- Ogg Vorbis解压缩.

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
