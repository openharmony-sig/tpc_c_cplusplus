# acl三方库说明
## 功能简介
acl是一个动画压缩库


## 三方库版本
- v2.1.0


## 已适配功能
- 提供动画压缩库能力


## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)


## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
