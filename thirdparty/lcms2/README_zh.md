# lcms2三方库说明
## 功能简介
Little cms is a color management library. Implements fast transforms between ICC profiles. It is focused on speed, and is portable across several platforms.

## 三方库版本
- 2.15

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
