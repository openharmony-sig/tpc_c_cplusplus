# JRTPLIB三方库说明
## 功能简介
该库提供了对RFC 3550中定义的实时传输协议（RTP）的支持

## 三方库版本
- v3.11.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
