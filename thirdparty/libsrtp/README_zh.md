# libsrtp三方库说明
## 功能简介
libSRTP提供了保护RTP和RTCP的功能。RTP数据包可以进行加密和身份验证（使用srtp_protect（）函数），将其转换为srtp数据包。

## 三方库版本
- v2.5.0 

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
