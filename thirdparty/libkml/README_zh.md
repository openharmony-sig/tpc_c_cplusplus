# libkml三方库说明
## 功能简介
libkml 是一个开源的 C++ 库，用于解析和生成 KML（Keyhole Markup Language）文件。KML 是一种基于 XML 的文件格式，用于表示地理空间数据，并广泛应用于 Google Earth、Google Maps 等地理信息系统。

## 三方库版本
- 916a801ed3143ab82c07ec108bad271aa441da16

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](docs/hap_integrate.md)
