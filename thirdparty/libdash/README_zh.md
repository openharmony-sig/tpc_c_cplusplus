# libdash三方库说明
## 功能简介
libdash是ISO/IEC MPEG-DASH标准的官方参考软件，为Bitmovin开发的MPEG-DASH提供面向对象（OO）接口。

## 三方库版本
- v2.2

## 已适配功能
- 支持MPEG-DASH编码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
