#  concurrentqueue 三方库说明

## 功能简介
concurrentqueue是一个高效的线程安全的队列的库。

## 三方库版本
- v1.0.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](docs/hap_integrate.md)
