# alsa-lib 三方库说明
## 功能简介
alsa-lib是一个用于操作音频设备的库，它提供了一组API，使得应用程序可以与音频设备进行交互。

## 三方库版本
- v1.1.3

## 已适配功能
- 音频采集、音频播放、音频混合等，还支持音频格式转换和音频效果处理等功能。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)