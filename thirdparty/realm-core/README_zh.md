# realm-core三方库说明
## 功能简介
Realm Core是一个C++编写的底层数据库引擎，它提供了对多种编程语言（如Java、Swift、Objective-C等）的支持。通过封装C++库，开发者可以轻松地在自己的应用中集成和使用Realm Core。

## 三方库版本
- v14.12.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
