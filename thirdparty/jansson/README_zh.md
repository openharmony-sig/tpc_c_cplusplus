# jansson三方库说明
## 功能简介
Jansson是一个用于解码、编码、操控JSON的C库。

## 三方库版本
- v2.14

## 已适配功能
- 解码、编码、操控JSON格式的数据，将其他类型的数据转化成JSON格式的数据。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)