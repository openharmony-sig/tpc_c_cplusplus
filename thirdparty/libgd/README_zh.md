# LIBGD 三方库说明
## 功能简介
用于动态创建图像

## 三方库版本
- v2.3.4

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
