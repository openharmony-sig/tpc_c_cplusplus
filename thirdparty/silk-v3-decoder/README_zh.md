# silk-v3-decoder三方库说明
## 功能简介
对silk v3音频文件进行软解或格式转换

## 三方库版本
- 07bfa0f56bbfcdacd56e2e73b7bcd10a0efb7f4c

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
