# libavc三方库说明
## 功能简介
libavc处理音频和视频

## 三方库版本
- v1.4.0

## 已适配功能
- 音视频处理

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
