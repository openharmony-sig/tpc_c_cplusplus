# resiprocate 三方库说明
## 功能简介
resiprocate实现SIP、ICE、TURN 和相关协议。

## 三方库版本
- v1.12.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
