# double-conversion三方库说明
## 功能简介
double-conversion用于IEEE高效二进制-十进制和十进制-二进制转换。

## 三方库版本
- v3.2.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
