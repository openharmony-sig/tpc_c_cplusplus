# libheif三方库说明
## 功能简介
libheif是HEIF和AVIF文件格式编解码三方库。

## 三方库版本
- v1.15.2

## 已适配功能
- 支持HEIF和AVIF文件格式编解码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
