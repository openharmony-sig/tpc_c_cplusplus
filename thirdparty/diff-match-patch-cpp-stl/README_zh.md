# diff-match-patch-cpp-stl三方库说明
## 功能简介
Diff Match Patch是一个多种语言的高性能库，可操作纯文本。

## 三方库版本
- master

## 已适配功能
- 支持文本对比等功能

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
