# tassl 三方库说明
## 功能简介
TASSL 是一个支持国密算法（SM2、SM3、SM4等）的SSL/TLS协议实现。SSL/TLS 协议用于在网络通信中提供加密和安全性。

## 三方库版本
- a30d107669fdf7d6909b10f1e179de4aaff38ff5
## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
