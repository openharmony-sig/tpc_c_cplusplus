# libid3tag三方库说明
## 功能简介
libid3tag是一个用于读取和写入ID3标签的库

## 三方库版本
- 0.15.1b

## 已适配功能
- 读取和写入ID3标签

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)