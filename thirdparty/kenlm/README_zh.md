# kenlm三方库说明
## 功能简介
- Kenlm 是一个用于构建和使用语言模型的工具.

## 三方库版本
- e504a4d61c413873e7d5f13d8ac4890bd4dae36c

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
