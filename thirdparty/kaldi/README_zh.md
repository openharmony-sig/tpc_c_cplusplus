# kaldi三方库说明
## 功能简介
Kaldi是开源语音识别工具(Toolkit)，它使用WFST来实现解码算法。

## 三方库版本
- kaldi10

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
