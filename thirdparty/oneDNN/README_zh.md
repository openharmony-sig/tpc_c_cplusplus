# oneDNN三方库说明
## 功能简介
oneDNN是为深度学习应用开发的一块跨平台开源库。

## 三方库版本
- v3.2

## 已适配功能
- 提供CNN基元、RNN原语、元素操作、支持多种函数和优化的数据布局。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)