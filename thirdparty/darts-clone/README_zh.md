# darts-clone三方库说明
## 功能简介
- darts-clone是一个头文件库，是一个在 GitHub 上由 Susumu Yata 维护的开源项目，该项目是对 DARTS（Double-ARray Trie System）的克隆或重新实现。DARTS 是一种基于双数组（double array）结构的 Trie 树系统，用于高效地存储和检索字符串集合。

## 三方库版本
- darts-clone-e40ce4627526985a7767444b6ed6893ab6ff8983

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
