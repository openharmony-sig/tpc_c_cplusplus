# exiv2三方库说明
## 功能简介
exiv2是一个C++库和命令行实用程序，用于读取、写入、删除和修改Exif、IPTC、XMP和ICC图像元数据。

## 三方库版本
- v0.27.6

## 已适配功能
- 读取、写入、删除和修改Exif、IPTC、XMP和ICC图像元数据

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
