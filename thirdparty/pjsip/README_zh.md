# pjsip三方库说明
## 功能简介
pjsip是一个免费的开源多媒体通信库，以C语言编写，实现了基于标准的协议，如SIP、SDP、RTP、STUN、TURN和ICE。它将信令协议（SIP）与丰富的多媒体框架和NAT穿透功能相结合，形成高级API.

## 三方库版本
- 2.13.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
