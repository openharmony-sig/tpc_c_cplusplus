# rapidjson三方库说明
## 功能简介
RapidJson是一个跨平台的c++的json的解析器和生成器。

## 三方库版本
- v1.1.0

## 已适配功能
- 支持json数据的解析和生成。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用包hap集成](docs/hap_integrate.md)
