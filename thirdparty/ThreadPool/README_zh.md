# ThreadPool 三方库说明
## 功能简介
ThreadPool是一个简单的c++线程池实现。

## 三方库版本
- 9a42ec1329f259a5f4881a291db1dcb8f2ad9040

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
