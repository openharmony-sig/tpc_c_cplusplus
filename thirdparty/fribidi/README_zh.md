# fribidi三方库说明
## 功能简介
fribidi是Unicode双向算法的实现库。

## 三方库版本
- v1.0.12

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
