# libssh2三方库说明
## 功能简介
libssh2是一个实现SSH2协议的库，可以在修订后的BSD许可下使用。

## 三方库版本
- 1.11.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
