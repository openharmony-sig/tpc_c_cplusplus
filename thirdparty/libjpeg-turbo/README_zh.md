# libjpeg-turbo 三方库说明

## 功能简介
libjpeg-turbo是一种JPEG图像编解码器，它使用SIMD指令来加速基准JPEG压缩和解压缩.

## 三方库版本
- 2.1.91

## 已适配功能
- 支持JPEG图像编解码。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
