# libuuid三方库说明
## 功能简介
The libuuid library is used to generate unique identifiers for objects that may be accessible beyond the local system. 

## 三方库版本
- 1.0.3

## 已适配功能
- 生成唯一识别码

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
