# apr三方库说明
## 功能简介
apr是一个是创建和维护软件库，提供一组映射到下层操作系统的API。

## 三方库版本
- 1.7.4

## 已适配功能
- 内存管理和内存池，原子操作，动态库处理等

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)