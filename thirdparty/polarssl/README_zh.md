# polarssl三方库说明
## 功能简介
PolarSSL库是SSL和TLS协议以及各自加密算法的实现。

## 三方库版本
- 1.4

## 已适配功能
- 加密和解密，包括对称加密（如AES、DES）、非对称加密（如RSA、ECC）和哈希函数
- 支持TLS安全通信协议
- X.509证书管理
- 安全随机数生成

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [系统hap包集成](docs/hap_integrate.md)
