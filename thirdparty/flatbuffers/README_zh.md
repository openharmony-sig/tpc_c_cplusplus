# flatbuffers 三方库说明
## 功能简介
flatbuffers 是一个跨平台的序列化库，其架构能最大限度地提高内存效率。

## 三方库版本
- v23.5.9

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
