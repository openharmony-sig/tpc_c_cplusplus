# libexpat三方库说明
## 功能简介
libexpat是一个用于解析XML 1.0的C99库，面向流的XML解析器。

## 三方库版本
- R_2_5_0

## 已适配功能
- 解析XML

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
