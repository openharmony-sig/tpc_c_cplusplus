# capnproto三方库说明
## 功能简介
Cap‘n Proto是一个基于能力速度极快的数据交换格式的RPC系统

## 三方库版本
- v1.0.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
