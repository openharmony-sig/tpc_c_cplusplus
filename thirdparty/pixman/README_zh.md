# pixman三方库说明
## 功能简介
Pixman是一个用于像素操作的低级软件库，提供图像合成和梯形光栅化等功能。

## 三方库版本
- 0.42.2 

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
