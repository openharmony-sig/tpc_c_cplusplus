# PROJ 三方库说明

## 功能简介

PROJ 是一个通用的坐标转换软件，它可以将坐标从一种坐标参考系(CRS)转换到另一种坐标参考系(CRS)。

## 三方库版本
- 9.4.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
