# tinyxpath三方库说明
## 功能简介
tinyxpath用于从 XML 树中提取 XPath 1.0 表达式。

## 三方库版本
- 1.3.1

## 已适配功能
- 支持xpath语法解析

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
