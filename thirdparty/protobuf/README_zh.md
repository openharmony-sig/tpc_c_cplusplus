# protobuf三方库说明
## 功能简介
protobuf是Google提供的一套数据的序列化框架。

## 三方库版本
- v4.23.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)