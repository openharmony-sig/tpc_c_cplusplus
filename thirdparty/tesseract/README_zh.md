# tesseract三方库说明
## 功能简介
tesseract是一个OCR引擎，提供库函数和命令行工具。

## 三方库版本
- 5.3.2
## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)