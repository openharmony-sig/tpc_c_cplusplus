# MNN三方库说明
## 功能简介
MNN是一个高效、轻量级的深度学习框架,支持深度学习模型的推理和训练

## 三方库版本
- 2.6.3

## 已适配功能
- 支持Tensorflow、Caffe和ONNX等不同的训练框架;通过算子融合、算子替代、模型压缩、布局调整等方式对图进行基本的优化操作

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)