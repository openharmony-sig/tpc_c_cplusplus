# fdk-aac三方库说明
## 功能简介
fdk-aac是一个开源的AAC编码库，被认为是开源AAC编码器中音质最好的之一。它支持多种编码模式，包括LC-AAC、HE-AAC和HE-AAC V2

## 三方库版本
- v2.0.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
