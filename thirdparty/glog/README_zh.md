# glog三方库说明
## 功能简介
提供基于C++样式流的日志记录API。

## 三方库版本
- v0.6.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
