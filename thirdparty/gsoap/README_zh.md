# gSOAP Toolkit三方库说明
## 功能简介
  gSOAP Toolkit是一个用于开发SOAP和XML应用的工具包，它在英文中被称为toolkit。该工具包跨平台，可用于开发webservice的客户端和服务器端。

## 三方库版本
- gsoap_2.8.134

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
