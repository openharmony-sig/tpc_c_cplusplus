# cairo三方库说明
## 功能简介
 cairo是一个2D图形库，支持多种输出设备。
 
## 三方库版本
- cairo-1.17.8

## 已适配功能
- 支持PDF图形输出、支持SVG图形输出、支持image图形输出

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)



