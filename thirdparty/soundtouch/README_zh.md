# soundtouch三方库说明
## 功能简介
soundtouch是一个提供音频变速变调能力的库。

## 三方库版本
- 2.3.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
