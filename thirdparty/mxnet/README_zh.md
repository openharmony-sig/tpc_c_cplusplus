# mxnet三方库说明
## 功能简介
mxnet是一个轻量级、便携、灵活的分布式/移动深度学习框架

## 三方库版本
- 1.9.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)