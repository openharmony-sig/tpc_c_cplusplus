# nghttp3三方库说明
## 功能简介
nghttp3是在C中通过QUIC和QPACK进行HTTP/3映射的实现，它不依赖于任何特定的QUIC传输实现

## 三方库版本
- v0.10.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
