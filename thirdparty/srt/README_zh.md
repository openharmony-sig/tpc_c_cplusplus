# srt三方库说明

## 功能简介

 srt是新一代低延迟视频传输协议的三方库

## 三方库版本
- 1.5.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
 [应用包hap集成](docs/hap_integrate.md)
