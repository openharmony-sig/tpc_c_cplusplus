# FFmpeg三方库说明
## 功能简介
  FFmpeg是领先的多媒体框架，能够解码、编码、转码、mux、demux、流式传输、过滤和播放人类和机器创建的任何东西，
可以运行在linux，mac，windows等平台；

## 三方库版本
- n6.0

## 已适配功能
- 解码，编码，转码，复用，解复用，过滤音视频数据

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
