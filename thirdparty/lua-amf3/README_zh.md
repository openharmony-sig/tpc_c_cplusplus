# lua-amf3三方库说明
## 功能简介
lua-amf3为lua提供AMF二进制格式数据编解码功能。

## 三方库版本
- 2.0.5

## 已适配功能
- 读取解析xml

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
