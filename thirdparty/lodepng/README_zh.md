# lodepng三方库说明
## 功能简介
lodepng是一个PNG编解码库

## 三方库版本
- v2.1.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
