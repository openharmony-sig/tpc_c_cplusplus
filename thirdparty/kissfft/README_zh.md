# kissfft三方库说明
## 功能简介
kissfft是提供快速傅立叶变换算法能力的库。

## 三方库版本
- v1.0.2

## 已适配功能
- 支持开源密码算法

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
