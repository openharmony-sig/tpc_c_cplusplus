# libmdbx 三方库说明
## 功能简介
libmdbx是一个非常快速、紧凑、强大的嵌入式事务性键值数据库，具有宽松的许可证。有一组特定的属性和功能，专注于创建独特的轻量级解决方案。

## 三方库版本
- 0.11.6

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
