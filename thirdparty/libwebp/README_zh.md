# libwebp 三方库说明
## 功能简介
WebP 编解码器是一个用于编码和解码 WebP 格式图像的库。该软件包包含可用于其他程序以添加WebP支持的库，以及分别用于压缩和解压缩图像的命令行工具“cwebp”和“dwebp”。

## 三方库版本
- v1.3.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
