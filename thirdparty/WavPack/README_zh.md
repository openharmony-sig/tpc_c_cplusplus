# WavPack三方库说明
## 功能简介
WavPack是一个无损音频压缩的库。

## 三方库版本
- 5.6.0

## 已适配功能
- 音频压缩

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
