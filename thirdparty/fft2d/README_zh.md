# fft2d 三方库说明
## 功能简介
fft2d 这是一个用于计算长度为2^N的一维序列的离散傅立叶/余弦/正弦变换的程序包。

## 三方库版本
- v2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
