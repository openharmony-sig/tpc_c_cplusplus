# libtorrent三方库说明
## 功能简介
libtorrent 是一个用于 BitTorrent 协议的高性能、跨平台的 C++ 库，用于文件共享的对等网络协议，允许用户从多个来源同时下载文件，从而实现快速和高效的文件分发。

## 三方库版本
- v2.0.10

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)