# libical三方库说明
## 功能简介
libical 是一个开源实现关于iCalendar协议和协议数据单元。

## 三方库版本
- v3.0.16

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)