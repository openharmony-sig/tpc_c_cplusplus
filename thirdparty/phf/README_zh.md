# phf三方库说明
## 功能简介
phf是实现完美hash算法的库。

## 三方库版本
- rel-20190215

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
