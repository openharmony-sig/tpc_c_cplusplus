# gflags三方库说明
## 功能简介
gflags是一种命令行解析工具，主要用于解析用命令行执行可执行文件时传入的参数。

## 三方库版本
- v2.2.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)
