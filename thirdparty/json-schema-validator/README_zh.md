# json-schema-validator三方库说明
## 功能简介
json-schema-validator用于验证基于JSON Schema的JSON文档。

## 三方库版本
- 2.2.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
