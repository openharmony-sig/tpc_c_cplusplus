# libmysofa三方库说明
## 功能简介
这是一组简单的C函数，用于读取AES SOFA文件

## 三方库版本
- v1.3.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
