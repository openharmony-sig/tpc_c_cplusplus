# gmp三方库说明
## 功能简介
gmp是用于任意精度算术的运算库。

## 三方库版本
- v1.0.12

## 已适配功能
- 支持任意精度算术

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
