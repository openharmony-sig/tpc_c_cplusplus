# openjpeg 三方库说明

## 功能简介

OpenJPEG 是用 C 语言编写的开源 JPEG 2000 编解码器。

## 三方库版本
- v2.5.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用Hap包集成](./docs/hap_integrate.md).
