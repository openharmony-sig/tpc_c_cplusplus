# yara三方库说明

## 功能简介

 yara根据文本或二进制模式创建恶意软件识别特征值，然后根据特征字扫描应用程序，判别病毒侵入情况

## 三方库版本
- 4.5.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
 [应用包hap集成](docs/hap_integrate.md)





