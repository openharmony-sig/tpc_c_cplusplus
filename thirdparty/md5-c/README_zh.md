# md5-c三方库说明
## 功能简介
md5的全称是md5信息摘要算法，用于确保信息传输的完整一致。

## 三方库版本
- f3529b666b7ae8b80b0a9fa88ac2a91b389909c7

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_ingtegrate.md)