# shapelib三方库说明

## 功能简介

 shapelib是一个主要用于读写和操作ESRI Shapefile文件格式，包括.shp（空间几何信息）和.dbf（属性信息）文件。

## 三方库版本
- 1.6.0

## 已适配功能
- 用于读取.shp文件的空间几何信息和.dbf文件的属性信息

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
 [应用包hap集成](docs/hap_integrate.md)





