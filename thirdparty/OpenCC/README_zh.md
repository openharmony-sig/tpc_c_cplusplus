# OpenCC 三方库说明
## 功能简介
OpenCC (开放式汉字转换) 是一个开源的工程，用于繁体中文、简体中文和日文汉字之间的转换。

## 三方库版本
- OpenCC-ver.1.1.9

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
