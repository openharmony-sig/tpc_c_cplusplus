# epeg三方库说明
## 功能简介
  epeg是一个用于读取和处理JPEG图像中EXIF（Exchangeable Image File Format）数据的轻量级C语言库。EXIF数据通常包含有关图像的信息，如拍摄时间、相机型号、曝光设置等，使用epeg你可以方便地访问这些元数据，而无需处理复杂的JPEG文件结构或EXIF规范。

## 三方库版本
- v0.9.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)