# DragonBonesCPP 三方库说明
## 功能简介
DragonBones 是一套骨骼动画工具集，最早使用 Flash 和 ActionScript 3.0 语言开发，主要在 Flash 游戏中使用，目前在页游和手游项目中使用很广泛

## 三方库版本
- 56b6847252c0f610a1b07f30b137b1ec49b36226

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
