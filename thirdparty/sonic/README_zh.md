# sonic三方库说明
## 功能简介
sonic是一种用于加速或减慢音频算法的库。

## 三方库版本
- release-0.2.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
