# sha三方库说明
## 功能简介

sha是计算出一个数字消息所对应到的，长度固定的字符串（又称消息摘要）的算法。

## 三方库版本
- df4ed74db83ea9ebc1486c247d3855dde8224481

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
[应用包hap集成](docs/hap_integrate.md)
