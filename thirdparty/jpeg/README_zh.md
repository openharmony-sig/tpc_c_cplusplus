# jpeg三方库说明
## 功能简介
jpeg是JPEG图像压缩免费库。

## 三方库版本
- v9e

## 已适配功能
- 支持JPEG图像压缩

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
