# libmad 三方库说明
## 功能简介
libmad - MPEG 音频解码器库

## 三方库版本
- c2f96fa4166446ac99449bdf6905f4218fb7d6b5

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
