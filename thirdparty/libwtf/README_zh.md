# libwtf三方库说明
## 功能简介
libwtf是一个网络模版框架。

## 三方库版本
- webkitgtk-2.41.90

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
