# libvips 三方库说明
## 功能简介
libvips 是一个需求驱动的水平线程图像处理库。

## 三方库版本
- v8.14.5

## 已适配功能
- 涵盖算术、直方图、卷积、形态操作、频率过滤、颜色等操作，支持多种图像格式，包括JPEG，，TIFF，PNG等。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
