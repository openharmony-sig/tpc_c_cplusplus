# libyuv三方库说明
## 功能简介
libyuv是一个开源的图像处理库，提供了多种图像处理功能，包括图像格式转换、颜色空间转换、颜色调整、去噪、去雾、锐化、缩放等。

## 三方库版本
- c0031cfd95e131c7b11be41d0272455cc63f10f4

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
