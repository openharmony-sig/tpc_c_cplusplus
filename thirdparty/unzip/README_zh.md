# unzip三方库说明
## 功能简介
unzip为zip压缩文件的解压缩程序。

## 三方库版本
- 60

## 已适配功能
- 解压缩zip文件

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
