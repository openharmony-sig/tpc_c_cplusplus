# tolua_runtime三方库说明
## 功能简介
tolua_runtime是一个框架功能,提供Lua代码逻辑热更,快速开发库。

## 三方库版本
- 1.0.8.584

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
