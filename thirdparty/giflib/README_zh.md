# giflib三方库说明
## 功能简介
giflib是一个用于阅读和编写gif图像的库

## 三方库版本
- 5.2.1

## 已适配功能
- 阅读和编写gif图像

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
- [应用hap包集成](docs/hap_integrate.md)
