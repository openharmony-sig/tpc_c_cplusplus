# cryptopp三方库说明
## 功能简介
cryptopp是开源的C++密码学库，集成了非常多的密码算法。

## 三方库版本
- Crypto++ 8.7 release

## 已适配功能
- 支持开源密码算法

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
