# libwbxml三方库说明
## 功能简介
libwbxml库是一个专门用于处理WBXML（Wireless Binary XML）文档的C语言编写的开源库，可以实现wbxml和xml格式之间的相互转换。

## 三方库版本
- 0.11.10

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)