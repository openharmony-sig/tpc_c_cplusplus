# libass三方库说明
## 功能简介
libass库则是一个轻量级的对ASS/SSA格式字幕进行渲染的开源库。

## 三方库版本
- 0.17.1

## 已适配功能
- 支持ASS/SSA格式字幕进行渲染

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
