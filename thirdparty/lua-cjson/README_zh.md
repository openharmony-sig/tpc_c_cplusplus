# lua-cjson三方库说明
## 功能简介
lua-cjson用于完成Lua值与Json值的相互转换（编码及解码）

## 三方库版本
- v2.1.0

## 已适配功能
- 支持Lua值与Json值的相互转换，lua-cjson要求编码格式为UTF8，不支持UTF-16和UTF-32

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
