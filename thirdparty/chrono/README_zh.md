# chrono三方库说明
## 功能简介
chrono是一个多物理场开源框架.

## 三方库版本
- 8.0.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
