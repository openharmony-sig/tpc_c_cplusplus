# thrax三方库说明
## 功能简介
OpenGrm-Thrax工具将表示为正则表达式的语法和上下文相关重写规则编译为加权有限状态转换器

## 三方库版本
- 1.3.9

## 已适配功能
- 语言模型

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
