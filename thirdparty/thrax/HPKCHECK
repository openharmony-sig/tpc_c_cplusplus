# Copyright (c) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Contributor: Jeff Han <hanjinfei@foxmail.com>, wen fan <wenfan001@chinasoftinc.com>
# Maintainer: Jeff Han <hanjinfei@foxmail.com>

source HPKBUILD > /dev/null 2>&1
logfile=${LYCIUM_THIRDPARTY_ROOT}/${pkgname}/${pkgname}_${ARCH}_${OHOS_SDK_VER}_test.log

checkprepare() {
    export PATH=${PATH}:${LYCIUM_ROOT}/usr/${pkgname}/${ARCH}/bin
    cd ${builddir}/src/grammars

    # 生成测试文件
    makepath=`which make` || return -1
    ${makepath} > ${logfile} 2>&1 || return -1
    cd ${OLDPWD}
    return 0
}

openharmonycheck() {
    cd ${builddir}/src/grammars

    result=
    declare -i failnum=0
    declare -i passnum=0
    declare -i testnum=1

    test1="Well, I can't eat muffins in an agitated manner."
    test2="Mr. Ernest Worthing, B. 4, The Albany."
    test3="Lieutenant 1840,"
    test4="Uncle Jack!"

    result1="Well , I ca n't eat muffins in an agitated manner ."
    result2="Mr. Ernest Worthing , B. four , The Albany ."
    result3="Lieutenant eighteen forty ,"
    result4="Uncle Ernest !"

    echo -e "${test1}\n${test2}\n${test3}\n${test4}" > test.txt

    while IFS= read -r line
    do
        echo "test${testnum}  start: ${line}" >> ${logfile} 2>&1
        result=$(echo "${line}" | thraxrewrite-tester --far=example.far --rules=TOKENIZER)
        result=$(echo "${result:29}" | head -n 1)
        echo "test${testnum} result: ${result}" >> ${logfile} 2>&1
        if [[ ${result} == ${result1} || ${result} == ${result2} || ${result} == ${result3} || ${result} == ${result4} ]]
        then
            echo -e "\e[32mtest PASS\e[0m" >> ${logfile} 2>&1
            ((passnum++))
        else
            echo -e "\e[31mtest FAIL\e[0m" >> ${logfile} 2>&1
            ((failnum++))
        fi
        ((testnum++))
    done < test.txt

    echo -e "\e[32m===================================================\e[0m" >> ${logfile} 2>&1
    echo -e "ALL  4 tests" >> ${logfile} 2>&1
    echo -e "\e[32mPASS ${passnum} tests\e[0m" >> ${logfile} 2>&1
    echo -e "\e[31mFAIL ${failnum} tests\e[0m" >> ${logfile} 2>&1
    echo -e "\e[32m===================================================\e[0m" >> ${logfile} 2>&1

    rm -f test.txt
    cd ${OLDPWD}

    if [ ${failnum} -ne 0 ]
    then
        return -1
    else
        return 0
    fi
}
