# ytnef三方库说明
## 功能简介

ytnef是一个开源库，实现对tnef流的多种格式附件的解析支持。

## 三方库版本
- 2.1.2

## 已适配功能
- 解析tnef格式邮件并提取文本、音频和图片等多种格式的文件数据

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)
