# simple三方库说明
## 功能简介
simple 是一个支持中文和拼音的 sqlite3 fts5 拓展。它完整提供了 微信移动端的全文检索多音字问题解决方案 一文中的方案四，非常简单和高效地支持中文及拼音的搜索。

## 三方库版本
- v0.4.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

+ [应用hap包集成](docs/hap_integrate.md)
