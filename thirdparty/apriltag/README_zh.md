# apriltag三方库说明
## 功能简介
apriltag是机器人研究中常用的一种视觉基准系统。

## 三方库版本
- v3.4.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
