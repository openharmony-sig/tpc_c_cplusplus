# CavalierContours三方库说明
## 功能简介
用于偏移、交并补等组合等操作的 2D 多折段线库。

## 三方库版本
- 7a35376eb4c2d5f917d3e0564ea630c94137255e

## 已适配功能
- 提供2D轮廓线膨胀收缩、合并等操作

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
