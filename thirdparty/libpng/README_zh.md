# libpng三方库说明
## 功能简介
libpng是一款C语言编写的用来读写PNG文件的库。

## 三方库版本
- 1.6.39

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
