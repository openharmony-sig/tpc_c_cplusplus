# farmhash 三方库说明
## 功能简介
google的高性能哈希函数库farmhash，为字符串和其他数据提供哈希函数。

## 三方库版本
- farmhash-0d859a811870d10f53a594927d0d0b97573ad06d

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
