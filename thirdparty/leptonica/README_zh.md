# Leptonica 三方库说明

## 功能简介
Leptonica是一个开放源码的C语言库，它被广泛地运用于图像处理和图像分析。

## 三方库版本
- 1.83.1

## 已适配功能
- 扫描的图片“去污”、“提色”功能和图片转pdf功能

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [系统hap包集成](docs/hap_integrate.md)
