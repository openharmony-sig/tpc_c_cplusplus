# JasPer 三方库说明

## 功能简介
JasPer是一个用于图像编码和操作的软件集合.

## 三方库版本
- 4.0.0

## 已适配功能
- 支持图像编码和读写操作。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
