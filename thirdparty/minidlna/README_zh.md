# minidlna三方库说明
## 功能简介
minidlna（又名ReadyDLNA）是服务器软件，旨在完全兼容DLNA / UPnP-AV客户端。

## 三方库版本
- 1.3.3

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
