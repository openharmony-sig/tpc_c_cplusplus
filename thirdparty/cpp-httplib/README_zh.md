# cpp-httplib 三方库说明
## 功能简介
一个C++11单文件头跨平台HTTP/HTTPS库。

## 三方库版本
- v0.13.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
