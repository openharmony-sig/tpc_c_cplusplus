# irods三方库说明
## 功能简介
iRODS是一个开源的数据管理系统,旨在提供高性能、可扩展和安全的数据存储和访问功能。它特别适用于大规模、复杂的数据集，支持各种数据格式和访问模式。

## 三方库版本
- 4.3.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
