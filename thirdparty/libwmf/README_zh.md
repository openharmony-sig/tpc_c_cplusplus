# libwmf 三方库说明

## 功能简介

libwmf libwmf是一个用于解析和处理Windows媒体格式(WMF)文件的库。

## 三方库版本
- v0.2.13

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式

- [应用hap包集成](docs/hap_integrate.md)
