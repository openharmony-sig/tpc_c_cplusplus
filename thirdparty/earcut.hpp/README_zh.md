# earcut.hpp三方库说明
## 功能简介
 earcut.hpp是一个c++版本的earcut.js，一个快速的，只有头文件的多边形三角测量库。

## 三方库版本
- earcut.hpp-a299ad92b93b03aeab5d95195bb34bfc8f0b3263

## 已适配功能
- 支持三角形的测量能力、支持三角形的剖分能力。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)



