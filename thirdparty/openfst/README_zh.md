# openfst三方库说明
## 功能简介
openfst 是一个开源的有限状态转换（FST）库，主要用于构建、操作和分析有限状态自动机和有限状态转换。

## 三方库版本
- 1.8.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [系统hap包集成](docs/hap_integrate.md)
