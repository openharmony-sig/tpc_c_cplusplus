# rime三方库说明
## 功能简介
  rime是一款开源的输入法引擎框架。

## 三方库版本
- 1.13.0

## 已适配功能
- 支持rime库的功能。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
