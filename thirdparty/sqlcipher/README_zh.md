# sqlcipher三方库说明
## 功能简介
SQLCipher 是 SQLite 的一个独立分支，它增加了数据库文件的 256 位 AES 加密和其他安全功能。

## 三方库版本
- 4.5.5

## 已适配功能
- SQLite数据库加密

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)

