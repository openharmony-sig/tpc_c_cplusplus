# libunibreak 三方库说明
## 功能简介
libunibreak是一个文本处理器。

## 三方库版本
- libunibreak_5_1

## 已适配功能
- Libunibreak 是Unicode 标准附件 14和Unicode 标准附件 29中描述的换行和分词算法的实现。它旨在用于通用文本渲染器。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
