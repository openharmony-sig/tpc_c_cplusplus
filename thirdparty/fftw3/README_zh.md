# fftw3三方库说明
## 功能简介
fftw3是一个快速计算离散傅里叶变换的标准C语言程序集。

## 三方库版本
- 3.3.10

## 已适配功能
- 计算离散傅里叶变换

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
