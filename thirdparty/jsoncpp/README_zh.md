# jsoncpp三方库说明
## 功能简介
jsoncpp是一个C++库，允许操作JSON值，包括对字符串的序列化和反序列化。

## 三方库版本
- 1.9.5

## 已适配功能
- 提供JSON序列化和反序列化能力

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
