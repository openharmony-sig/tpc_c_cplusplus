# libtess2三方库说明
## 功能简介
libtess2 可以对复杂多边形进行曲面细分。

## 三方库版本
- 1.3.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
