# rsync 三方库说明
## 功能简介
rsync是一个开源、快速、多功能的工具，用于实现本地或远程数据的全量或增量同步备份。它可以在多种操作系统平台上运行。

## 三方库版本
- v3.0.0

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
