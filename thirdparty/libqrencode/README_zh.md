# libqrencode三方库说明
## 功能简介
Libqrencode是一个快速紧凑的库，用于将数据编码为二维码，二维码是一种二维符号，可以通过智能手机等方便的终端进行扫描。

## 三方库版本
- v4.1.1

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
