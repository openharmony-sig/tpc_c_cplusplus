# ceres-solver三方库说明
## 功能简介
ceres-solver是一个由Google开发的开源C++库，用于解决具有边界约束和非线性最小二乘问题的优化问题，以及一般无约束优化问题。

## 三方库版本
- 2.1.0

## 已适配功能
- 解决具有边界约束和非线性最小二乘问题的优化问题.

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
