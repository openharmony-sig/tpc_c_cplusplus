# assimp三方库说明
## 功能简介
assimp作为一个开源项目，设计了一套可扩展的架构，为模型的导入导出提供了良好的支持。

## 三方库版本
- 5.2.5

## 已适配功能
- 支持导入导出3D模型数据

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
