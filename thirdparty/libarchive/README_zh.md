# libarchive 三方库说明
## 功能简介
libarchive项目开发了一种便携式，高效的C库，可以以各种格式读取和编写流库。它还包括使用libarchive库的常见功能包括，tar,cpio 和 zcat 命令行工具的实现。

## 三方库版本
- v3.6.2

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
