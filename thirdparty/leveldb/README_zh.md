# leveldb三方库说明
## 功能简介
  leveldb是一款快速键值存储库，它提供了从字符串键到字符串值的有序映射。

## 三方库版本
- 1.23

## 已适配功能
- 支持leveldb的增删改查等数据库功能。

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
