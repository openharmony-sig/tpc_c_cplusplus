# pcre2三方库说明
## 功能简介

pcre2是升级版的支持 Perl 语法的正则表达式库，能够用于处理文本匹配、搜索和替换等操作。

## 三方库版本
- 10.42

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

 ## 集成方式
[应用包hap集成](docs/hap_integrate.md)
