# mythes三方库说明
## 功能简介
mythes是一个简单的词库，使用结构化的具有二进制搜索的文本数据文件和索引文件查找单词和短语并返回词性、意义和同义词。

## 三方库版本
- 0.9

## 使用约束
- [IDE和SDK版本](../../docs/constraint.md)

## 集成方式
+ [应用hap包集成](docs/hap_integrate.md)
